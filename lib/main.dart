import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import './home/home_page.dart';
import './colors.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Dollar Calculator',
      theme: buildThemeData(),
      home: MyHomePage(title: 'Dollar Calculator'),
    );
  }
}

ThemeData buildThemeData() {
  final baseTheme = ThemeData.light();

  return baseTheme.copyWith(
    primaryColor: kPrimaryColor,
    primaryColorLight: kPrimaryColorLight,
    primaryColorDark: kPrimaryColorDark,
    accentColor: kPrimaryColorLight,
  );
}
