import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'dart:async';

import '../menu.dart';
import '../hitoric/file_manager.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  double _bolivares = 0.0;
  double _dollars = 0.0;
  double _dollarValue = 1.0;
  bool visibilityTag = false;
  bool justPay;

  final _key = GlobalKey<FormState>();
  final _cont = TextEditingController();

  void initDollarValue() async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    setState(() {
      _dollarValue = storage.getDouble('dollarValue') ?? 1.0;
    });
  }

  void _incrementCounter() {
    setState(() {
      if (_key.currentState.validate()) {
        _key.currentState.save();
        visibilityTag = true;
        if (justPay == null) {
          justPay = false;
        } else if (justPay == false) {
          justPay = true;
        }
      }
    });

    Timer(Duration(seconds: 15), () {
      setState(() {
        if (justPay == false) {
          justPay = null;
          visibilityTag = false;
        } else if (justPay == true) {
          justPay = false;
        }
      });
    });
  }

  void _saving(value) {
    _bolivares = double.parse(value);
    _dollars = _bolivares / _dollarValue;
    _cont.text = "";
    FileManager.registerEvent(_bolivares, _dollarValue);
  }

  @override
  void initState() {
    super.initState();
    initDollarValue();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Menu(_dollarValue, initDollarValue),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(20.0),
              child: Form(
                key: _key,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      controller: _cont,
                      style: Theme.of(context).textTheme.display1,
                      decoration: InputDecoration(
                          hintText: '0.00',
                          border: InputBorder.none,
                          labelText: 'Bolivares'),
                      keyboardType:
                          TextInputType.numberWithOptions(decimal: true),
                      validator: (value) {
                        if (double.parse(value) < 0.01) {
                          return "This number most be greater than one cent.";
                        }
                      },
                      onSaved: _saving,
                    ),
                  ],
                ),
              ),
            ),
            DisplayAmounts(
                bolivares: _bolivares,
                dollars: _dollars,
                visibilityTag: visibilityTag)
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class DisplayAmounts extends StatelessWidget {
  final double bolivares;
  final double dollars;
  final bool visibilityTag;
  final formatter = NumberFormat("#,###.##");

  DisplayAmounts({this.bolivares, this.dollars, this.visibilityTag});

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: visibilityTag ? 1.0 : 0.0,
      duration: Duration(milliseconds: 300),
      child: Column(
        children: <Widget>[
          Text(
            'You just spend:',
          ),
          Text(
            'Bs' +
                formatter.format(bolivares) +
                ' (\$' +
                formatter.format(dollars) +
                ')',
            style: Theme.of(context).textTheme.display1,
          )
        ],
      ),
    );
  }
}
