import 'package:flutter/material.dart';

const kPrimaryColor = const Color(0xFF546e7a);
const kPrimaryColorLight = const Color(0xFF819ca9);
const kPrimaryColorDark = const Color(0xFF29434e);
const kSecondaryColor = const Color(0xFF212121);
const kSecondaryColorLight = const Color(0xFF484848);
const kSecondaryColorDark = const Color(0xFF000000);