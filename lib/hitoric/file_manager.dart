import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'package:path_provider/path_provider.dart';
import 'package:intl/intl.dart';

class FileManager {
  static File _jsonFile;
  static Directory _dir;
  static String _fileName = 'hitoric.json';
  static Map<String, dynamic> _contentMap;
  static HistoricList _fullHistoric;

  static Future<File> _createFile() async {
    File file = File(_dir.path + '/' + _fileName);
    file.create();
    return file;
  }

  static void deleteHistoric() async {
    _dir = await getApplicationDocumentsDirectory();
    _jsonFile = File(_dir.path + '/' + _fileName);
    if (await _jsonFile.exists()) {
      _jsonFile.delete();
    }
  }

  static Future<HistoricList> getEventList() async {
    _dir = await getApplicationDocumentsDirectory();
    _jsonFile = File(_dir.path + '/' + _fileName);
    if (!await _jsonFile.exists()) {
      await _createFile();
    }
    if (await _jsonFile.readAsString() == '') {
      print(await _jsonFile.readAsString());
      return null;
    }
    _contentMap = json.decode(await _jsonFile.readAsString());
    _fullHistoric = HistoricList.fromJson(_contentMap);
    return _fullHistoric;
  }

  static Future<void> registerEvent(bolivares, dollarValue) async {
    HistoricList auxList = await getEventList();
    if (auxList == null) {
      auxList = HistoricList(events: []);
    }
    HistoricEvent auxObj = HistoricEvent(
        bolivarAmount: bolivares,
        dollarAmount: bolivares / dollarValue,
        dollarValue: dollarValue,
        date: DateTime.now());
    auxList.events.add(auxObj);
    _dir = await getApplicationDocumentsDirectory();
    _jsonFile = File(_dir.path + '/' + _fileName);
    if (!_jsonFile.existsSync()) {
      await _createFile();
    }
    _jsonFile.writeAsString(json.encode(auxList));
  }

  static Future<HistoricList> deleteEvent(DateTime date) async {
    HistoricList list = await getEventList();
    for(int i = 0; i < list.events.length; i++){
      DateTime auxDate = DateTime.parse(list.events[i]['date']);
      if(auxDate == date){
        list.events.removeAt(i);
        if (!_jsonFile.existsSync()) {
          await _createFile();
        }
        _jsonFile.writeAsString(json.encode(list));
      }
    }
    return list;
  }
}

class HistoricList {
  List<dynamic> events;

  HistoricList({this.events});

  factory HistoricList.fromJson(Map<String, dynamic> parsedJson) {
    return HistoricList(
      events: parsedJson['events'],
    );
  }

  Map toJson() {
    Map map = new Map();
    map["events"] = events;
    return map;
  }
}

class HistoricEvent {
  double dollarValue;
  double dollarAmount;
  double bolivarAmount;
  final DateTime date;

  HistoricEvent(
      {this.dollarValue, this.dollarAmount, this.bolivarAmount, this.date});

  factory HistoricEvent.fromJson(Map<String, dynamic> parsedJson) {
    return HistoricEvent(
      dollarValue: parsedJson['dollarValue'],
      dollarAmount: parsedJson['dollarAmount'],
      bolivarAmount: parsedJson['bolivarAmount'],
      date: DateTime.parse(parsedJson['date']),
    );
  }

  Map toJson() {
    Map map = new Map();
    map["dollarValue"] = dollarValue;
    map["dollarAmount"] = dollarAmount;
    map["bolivarAmount"] = bolivarAmount;
    map["date"] = date.toIso8601String();
    return map;
  }
}
