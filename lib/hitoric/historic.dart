import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

import './file_manager.dart';

class HistoricView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HistoricView();
  }
}

class _HistoricView extends State<HistoricView> {
  HistoricList historicList = HistoricList(events: []);
  bool visibilityTag = false;
  int touchCounter = 7;
  bool _justClick;

  void deleteHistoric() {
    setState(() {
      touchCounter--;
      if (touchCounter != 7 && touchCounter != 0) {
        touchCounter < 5 ? visibilityTag = true : visibilityTag = false;
        if (_justClick == null) {
          _justClick = false;
        } else if (_justClick == false) {
          _justClick = true;
        }
      } else if (touchCounter == 0) {
        touchCounter = 7;
        _justClick = null;
        visibilityTag = false;
        FileManager.deleteHistoric();
        historicList = null;
      }
    });

    Timer(Duration(seconds: 3), () {
      setState(() {
        if (_justClick == false) {
          _justClick = null;
          touchCounter = 7;
          visibilityTag = false;
        } else if (_justClick == true) {
          _justClick = false;
        }
      });
    });
  }

  void deleteEvent(DateTime date) {
    FileManager.deleteEvent(date).then((list) {
      setState(() {
        historicList = list;
      });
    });
  }

  @override
  void initState() {
    print('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@');
    super.initState();
    FileManager.getEventList().then((HistoricList futureHistoricList) {
      setState(() {
        historicList = futureHistoricList;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              GestureDetector(
                child: Text("Historic"),
                onTap: () {
                  deleteHistoric();
                },
              ),
              AnimatedOpacity(
                  opacity: visibilityTag ? 1.0 : 0.0,
                  duration: Duration(milliseconds: 300),
                  child: Chip(
                    avatar: CircleAvatar(
                      backgroundColor: Colors.grey.shade800,
                      child: Text('$touchCounter'),
                    ),
                    label: Text('To delete historic'),
                  ))
            ],
          ),
        ),
        body: HistoricListView(historicList, deleteEvent, context));
  }
}

class HistoricListView extends StatelessWidget {
  final formatter = NumberFormat("#,###.##");
  final HistoricList historicList;
  final BuildContext context;
  final Function deleteEvent;

  // block Daily Resumes
  double currentDailyAmount = 0.0;
  double currentMonthlyAmount = 0.0;
  DateTime currentEventDate;

  // endblock

  HistoricListView(this.historicList, this.deleteEvent, this.context);

  Future<Null> _showEventDetail(dynamic event) async {
    final formatter = NumberFormat("#,###.##");
    final DateFormat dateFormatter = DateFormat('dd/MM - H:m');
    final double _dollarAmount = event['dollarAmount'];
    final double _bolivaresAmount = event['bolivarAmount'];
    final double _dollarValue = event['dollarValue'];
    final DateTime _date = DateTime.parse(event['date']);
    switch (await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text('Detail'),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(15.0),
                child: Table(
                  children: [
                    TableRow(children: [
                      Text("\$" + formatter.format(_dollarAmount)),
                      Text("Bs." + formatter.format(_bolivaresAmount))
                    ]),
                    TableRow(children: [
                      Text("Value: Bs." + formatter.format(_dollarValue)),
                      Text("Date: " + dateFormatter.format(_date))
                    ])
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(
                    child: Text("Delete"),
                    onPressed: () {
                      deleteEvent(_date);
                      Navigator.pop(context, 'delete');
                    },
                  ),
                  FlatButton(
                    child: Text("Ok"),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ],
          );
        })) {
      case 'delete':
        // Let's go.
        // ...
        break;
    }
  }

  List<Widget> setEventsToTiles(HistoricList hList) {
    List<Widget> tList = [];
    final DateFormat dateFormatter = DateFormat('dd/MM/yy - H:m');
    for (int i = 0; i < hList.events.length; i++) {
      DateTime currentEventDate = DateTime.parse(hList.events[i]['date']);

      ListTile aux = ListTile(
        title: Text('\$' + formatter.format(hList.events[i]['dollarAmount']),
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
        subtitle: Text(dateFormatter.format(currentEventDate)),
        leading: Icon(
          Icons.attach_money,
          color: Theme.of(context).primaryColorLight,
          size: 40.0,
        ),
        onTap: () {
          _showEventDetail(hList.events[i]);
        },
      );

      tList.add(aux);
      i == hList.events.length - 1
          ? tList = _addDailyResume(tList, hList.events[i], true)
          : tList = _addDailyResume(tList, hList.events[i], false);
    }
    return tList.reversed.toList();
  }

  List<Widget> _addDailyResume(
      List<Widget> list, Map<String, dynamic> initEvent, bool isLastOne) {
    HistoricEvent event = HistoricEvent.fromJson(initEvent);

    currentDailyAmount += event.dollarAmount;

    if ((currentEventDate != null &&
            event.date.weekday > currentEventDate.weekday) ||
        isLastOne) {
      list.add(Divider());
      list.add(ListTile(
        title: Text('Today \$' + formatter.format(currentDailyAmount),
            style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
        leading: Icon(
          Icons.weekend,
          color: Theme.of(context).accentColor,
          size: 40.0,
        ),
        onTap: () {
//          _showEventDetail(hList.events[i]);
        },
      ));
      list.add(Divider());
      currentDailyAmount = 0.0;
    }

    currentEventDate = event.date;

    return list;
  }

  @override
  Widget build(BuildContext context) {
    if (historicList == null) {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "No history for now",
              style: Theme.of(context).textTheme.display1,
            ),
            Icon(
              Icons.healing,
              size: 50.0,
            )
          ],
        ),
      );
    }
    return ListView(
      children: setEventsToTiles(historicList),
    );
  }
}
