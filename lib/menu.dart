import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import './price/set_price.dart';
import './hitoric/historic.dart';

class Menu extends StatelessWidget {
  final double dollarValue;
  final formatter = NumberFormat("#,###.##");
  Function valueDidChange;

  Menu(this.dollarValue, this.valueDidChange);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          ListTile(
            title: Text('Hitoric',
                style:
                TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
            subtitle: Text('429 Castro St'),
            leading: Icon(
              Icons.restore,
              color: Theme
                  .of(context)
                  .primaryColorLight,
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => HistoricView()));
            },
          ),
          ListTile(
            title: Text('Set dollar price',
                style:
                TextStyle(fontWeight: FontWeight.w500, fontSize: 20.0)),
            subtitle: Text('Current dollar price Bs' + formatter.format(dollarValue)),
            leading: Icon(
              Icons.attach_money,
              color: Theme
                  .of(context)
                  .primaryColorLight,
            ),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => DollarPrice(valueDidChange)));
            },
          ),
        ],
      ),
    );
  }

}