import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'dart:async';

class DollarPrice extends StatefulWidget {

  Function valueDidChange;

  DollarPrice(this.valueDidChange);

  @override
  State<StatefulWidget> createState() {
    return _DollarPriceState();
  }
}

class _DollarPriceState extends State<DollarPrice> {
  final _cont = TextEditingController();
  final formatter = NumberFormat("#,###.##");
  double _currentValue = 1.0;

  _setInitValue() async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    setState(() {
      _currentValue = storage.getDouble('dollarValue') ?? 1.0;
      _cont.text = _currentValue.toString();
    });
  }

  _setNewPrice(double value) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    setState(() {
      _currentValue = value;
      storage.setDouble('dollarValue', value);
      widget.valueDidChange();
    });
  }

  @override
  initState() {
    super.initState();
    _setInitValue();
  }

  Future<Null> _setNewDollarPrice() async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text('New value per Bs'),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(10.0),
                child: TextField(
                  controller: _cont,
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                ),
              ),
              SimpleDialogOption(
                child:
                    Align(child: Text("Set"), alignment: Alignment.centerRight),
                onPressed: () {
                  _setNewPrice(double.parse(_cont.text));
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Current dollar price"),
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("Current value."),
          Text(
            "Bs" + formatter.format(_currentValue) + "/\$",
            style: Theme.of(context).textTheme.display1,
          ),
          RaisedButton(
            onPressed: () {
              _setNewDollarPrice();
            },
            child: Text(
              "Set new value",
              style: TextStyle(color: Color(0xFFFFFFFF)),
            ),
            color: Theme.of(context).accentColor,
          )
        ],
      )),
    );
  }
}
